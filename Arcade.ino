const int dirDown = 7;
const int dirUp = 6;
const int dirRight = 5;
const int dirLeft = 4;
const int soco = A0;
const int chute = A1;
const int socobaxo = A2;
const int chutebaxo = A3;

void setup() {
  pinMode(dirDown, INPUT);
  pinMode(dirUp, INPUT);
  pinMode(dirRight, INPUT);
  pinMode(dirLeft, INPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  
  Serial.begin(9600);
}

void loop() {
  joystick();
  swith();
  delay(100);
}

void joystick() {
  if(digitalRead(dirDown) == HIGH){
    Serial.println("Direction: DOWN");
  }
  else if(digitalRead(dirUp) != HIGH){
    Serial.println("Direction: UP");
  }
  else if(digitalRead(dirLeft) != HIGH){
    Serial.println("Direction: LEFT");
  }
  else if(digitalRead(dirRight) != HIGH){
    Serial.println("Direction: RIGHT");
  }
  else{
    Serial.println("STOP");
  }
}

void swith() {
  if(digitalRead(soco) == HIGH){
    Serial.println("Direction: soco");
  }
  else if(digitalRead(chute) == HIGH){
    Serial.println("Direction: chute");
  }
  else if(digitalRead(socobaxo) == HIGH){
    Serial.println("Direction: socobaxo");
  }
  else if(digitalRead(chutebaxo) == HIGH){
    Serial.println("Direction: chutebaxo");
  }
  else{
    Serial.println("STOP");
  }
}
